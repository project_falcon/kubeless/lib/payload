package payload

import (
	"encoding/json"
	"time"
)

const (
	AGetIdTask = "agetidtask"

	// Storage
	// SIncrementBuildLastNumber set build last number
	SIncrementBuildLastNumber = "sincrementbuildlastnumber"
	// SGetBuildLastNumber get build last number
	SGetBuildLastNumber = "sgetbuildlastnumber"
	// SSetBuildStepStatus set build step status
	SSetBuildStepStatus = "SSetBuildStepStatus"
	// SGetBuildStepStatus get build step status
	SGetBuildStepStatus = "sgetbuildstepstatus"
	// SSetBuildCurrentStatus set build current status
	SSetBuildCurrentStatus = "ssetbuildcurrentstatus"
	// SGetBuildCurrentStatus get build current status
	SGetBuildCurrentStatus = "sgetbuildcurrentstatus"
	// SGetBuildsNumbers get all buils numbers
	SGetBuildsNumbers = "sgetbuildsnumbers"
	// SGetBuildAllStages get specific build all steps
	SGetBuildAllStages = "sgetbuildallstages"

	// moved to project
	// //SGetGitRepo get git repo
	// SGetGitRepo = "sgetgitrepo"
	// //SSetGitRepo set git repo
	// SSetGitRepo = "ssetgitrepo"

	// SGetEcrURL get ECR URL
	SGetEcrURL = "sgetecrurl"

	// SGetSuffixIngressDomain get suffix domain for Ingress
	SGetSuffixIngressDomain = "sgetsuffixingressdomain"

	// SGetProjectList get project list
	SGetProjectList = "sgetprojectlist"
	// SAddProject add new project
	SAddProject = "saddproject"
	// SDelProject delete project
	SDelProject = "sdelproject"
	// SGetProject get one project
	SGetProject = "sgetproject"

	// SSetGitSSHKey set GIT SSH key not fully implemented
	SSetSSHKey = "ssetsshkey"
	// SGetGitSSHKey get GIT SSH key
	SGetSSHKey = "sgetsshkey"
	// SDelGitSSHKey get GIT SSH key
	SDelSSHKey = "sdelsshkey"
	// SGetGitSSHKeyList get SSH key List
	SGetSSHKeyList = "sgetsshkeylist"
	//SSetGitRepo set git repo
	SSetGitRepo = "ssetgitrepo"
	//SGetGitRepo get git repo
	SGetGitRepo = "sgetgitrepo"
	//SDelGitRepo get git repo
	SDelGitRepo = "sdelgitrepo"
	//SGetGitRepoList get git repo list
	SGetGitRepoList = "sgetgitrepolist"

	// SGetYAMLBuildFile get YAML build file
	SGetYAMLBuildFile = "sgetyamlbuildfile"
	// SAddYAMLBuildFile add YAML build file
	SAddYAMLBuildFile = "saddyamlbuildfile"
	// SCheckYAMLBuildFile check if YAML file is aviable for project
	SCheckYAMLBuildFile = "scheckyamlbuildfile" /// hmmm to remove if we have  SGetYAMLBuildList ?
	// SGetYAMLBuildList get list of projects which have configured yaml file
	SGetYAMLBuildList = "sgetyamlbuildlist"
	// SDeleteYAMLBuild delete specific yaml build
	SDeleteYAMLBuild = "sdeleteyamlbuild"
	// //SGetYAMLDescription get description of containers
	// SGetYAMLDescription = "sgetyamldescription"

	// SGetContainers get description of containers
	SGetContainers = "sgetcontainers"
	// SCacheGetContainers get description of containers
	SCacheGetContainers = "scachegetcontainers"
	// SCachePutContainers get description of containers
	SCachePutContainers = "scacheputcontainers"

	// SGetBrandList get brand list
	SGetBrandList = "sgetbrandlist"
	// SAddBrand get brand list
	SAddBrand = "saddbrand"
	// SDelBrand delete brand list
	SDelBrand = "sdelbrand"

	// SCachePutBranches create cache for branches
	SCachePutBranches = "scacheputbranches"
	// SCacheGetBranches get branches from cache if aviable
	SCacheGetBranches = "scachegetbranches"

	// SSetTTLBox set TTL value for BOX expiration time
	SSetTTLBox = "ssetttlbox"
	// SGetTTLBox get TTL value for BOX expiration time
	SGetTTLBox = "sgetttlbox"
	// SGetServiceAccount get service account for KCreate
	SGetServiceAccount = "sgetserviceaccount"
	//SSetServiceAccount set service account for KCreate
	SSetServiceAccount = "ssetserviceaccount"
	//SSetBuildLimit set the build limit
	SSetBuildLimit = "ssetbuildlimit"
	//SGetBuildLimit get the build limit
	SGetBuildLimit = "sgetbuildlimit"
	//SSetMaintenanceMode set the Maintenance Mode
	SSetMaintenanceMode = "ssetmaintenancemode"
	//SGetMaintenanceMode get the Maintenance Mode
	SGetMaintenanceMode = "sgetmaintenancemode"

	// GIT

	// GGetBranchAll get list of all branches
	GGetBranchAll = "ggetbranchall"
	// GGetBranchOne get only specific branch
	GGetBranchOne = "ggetbranchone"
	// GUpdateBranchesCache update cache for all branches
	GUpdateBranchesCache = "gupdatebranchescache"

	// Queue

	// QGetWaitingJobs get numbers of wating jobs
	QGetWaitingJobs = "qgetwaitingjobs"
	// QGetJob get job from queue
	QGetJob = "qgetjob"
	// QAddJob add job to queue
	QAddJob = "qaddjob"
	//
	QDelQueue = "qdelqueue"
	// QListQueuesName get list of queues
	// QListQueuesName = "qlistqueuesname"

	// Queue Type <---

	// QueueKbox queue for put jobs to create kbox
	QueueKbox = "queue_kbox"
	// QueueUnitTest queue for put jobs to create unittests
	QueueUnitTest = "queue_test"
	// QueueDelete queue for put jobs to delete k8s resources
	QueueDelete = "queue_delete"

	// fvalidation

	// CCreateBox create BOX
	VValidateBox = "vvalidatebox"
	// CCreateBuild create build
	// VValidateBuild = "vvalidatebuild"  to remove  never will be used:  vvalidatebox-> ck8screatebuild
	// CCreateUnitTest create UnitTest
	VValidateUnitTest = "vvalidateunittest"

	//kcreate

	// CCreateBox create BOX
	CK8sCreateBox = "ck8screatebox"
	// CCreateBuild create build
	CK8sCreateBuild = "ck8screatebuild"
	// CCreateUnitTest create UnitTest
	CK8sCreateUnitTest = "ck8screateunittest"

	// DK8sCreateDeploy deploy box
	DK8sCreateDeploy = "dk8sdeploybox"
	// DK8sCreateDeploy deploy box
	DK8sUpdateDeploy = "dk8supdatedeploy"
	// // CCreateBuildFinished inform about finished job
	// CCreateBuildFinished = "ccreatebuildfinished"
	// // CCreateUnitTestFinished inform about finished job
	// CCreateUnitTestFinished = "ccreateunittestfinished"

	// CFcctionPrepareBuild      = "preparebuild"
	// CFcActionPrepareTest       = "preparetest"
	// CFcStatusPrepareInProgress = "InProgress"
	// CFcStatusPrepareSuccess    = "Success"
	// CFcStatusPrepareFail       = "Fail"
	// CFcStatusPrepareWaiting    = "Waiting"

	// CFcCreateBox Fc create BOX
	// CFcCreateBox = "cfccreatebox"

	// klist

	// LK8sGetListPods k8s get list of pods
	LK8sGetListPods = "lk8sgetlistpods"
	// LK8sGetListING k8s get list of ingresses
	LK8sGetListIngress = "lk8sgetlistingress"
	// LK8sGetListSVC k8s get list of services
	LK8sGetListService = "lk8sgetlistservice"
	// LK8sGetListAll k8s get list of all resources (pod svc ing)
	// LK8sGetListAll = "lk8sgetlistall"
	// LK8sGetClusterAll k8s get all resources belongs to the specific cluster
	LK8sGetClusterAll = "lk8sgetclusterall"
	// LK8sGetListNameSpace k8s get list of namespaces
	LK8sGetListNameSpace = "lk8sgetlistnamespace"
	// LK8sGetListConfigMap k8s get list of configmap
	LK8sGetListConfigMap = "lk8sgetlistconfigmap"
	// LK8sGetListSecrets k8s get list of secret
	LK8sGetListSecrets = "lk8sgetlistsecrets"
	//LK8sGetListDeploys k8s get list of deploys
	LK8sGetListDeploys = "lk8sgetlistdeploys"
	// LK8sGetListPods k8s get list of pods
	LK8sGetPod = "lk8sgetpod"
	// LK8sGetListING k8s get list of ingresses
	LK8sGetIngress = "lk8sgetingress"
	// LK8sGetListSVC k8s get list of services
	LK8sGetService = "lk8sgetservice"
	// LK8sGetClusterAll k8s get all resources belongs to the specific cluster
	LK8sGetCluster = "lk8sgetcluster"
	// LK8sGetListNameSpace k8s get list of namespaces
	LK8sGetNameSpace = "lk8sgetnamespace"
	// LK8sGetListConfigMap k8s get list of configmap
	LK8sGetConfigMap = "lk8sgetconfigmap"
	// LK8sGetListSecrets k8s get list of secret
	LK8sGetSecret = "lk8sgetsecret"
	//LK8sGetListDeploys k8s get list of deploys
	LK8sGetDeploy = "lk8sgetdeploy"

	//IK8sGetBuildList get list of builds
	IK8sGetBuildList = "ik8sgetbuildlist"
	//IK8sGetBuildStatus get status of single build
	IK8sGetBuildStatus = "ik8sgetbuildstatus"
	//IK8sGetDeployStatus get deploy status
	IK8sGetDeployStatus = "ik8sgetdeploystatus"
	//IK8sGetBuildLogs get logs of single build
	IK8sGetLogs = "ik8sgetlogs"
	//IK8sSetDeleteFlag set delete Flag
	IK8sSetDeleteFlag = "ik8ssetdeleteflag"
	//IK8sSetSuspend set suspend
	IK8sSetSuspend = "ik8ssetsuspend"
	//IK8sRemoveSuspend remove Suspend
	IK8sRemoveSuspend = "ik8sremovesuspend"

	// NK8sGetListNodes get lis on nodes
	NK8sGetListNodes = "nk8sgetlistnodes"

	// kdelete

	// DK8sDeletePOD k8s delete pod
	DK8sDeletePod = "dk8sdeletepod"
	// DK8sDeleteSVC k8s delete svc
	DK8sDeleteService = "dk8sdeleteservice"
	// DK8sDeleteING k8s delete ing
	DK8sDeleteIngress = "dk8sdeleteingress"
	// DK8sDeleteNS k8s delete Namespace
	DK8sDeleteNamespace = "dk8sdeletenamespace"
	// DK8sDeleteCM k8s delete ConfigMap
	DK8sDeleteConfigMap = "dk8sdeleteconfigmap"
	// DK8sDeleteSecret k8s delete ConfigMap
	DK8sDeleteSecret = "dk8sdeletesecret"
	// DK8sDeleteSecret k8s delete ConfigMap
	DK8sDeleteDeploy = "dk8sdeletedeploy"
	// DK8sDeleteAll k8s delete all resources (pod svc ing)
	DK8sDeleteAll = "dk8sdeleteall"

	// kterminiate

	// TK8sCheckTTL k8s check and put pods to be deleted if TTL expired
	TK8sCheckTTL = "tk8scheckttl"

	//https://pkg.go.dev/k8s.io/api/core/v1#NodeConditionType
	// PressureReady metric
	PressureReady = "Ready"
	// PressureMemory metric
	PressureMemory = "MemoryPressure"
	// PressureDisk metric
	PressureDisk = "DiskPressure"
	// PressurePID metric
	PressurePID = "PIDPressure"

	//JOBType

)

// APIData default format of data
type APIData struct {
	Action  string   `json:"action,omitempty"`
	IDtask  string   `json:"idtask,omitempty"`
	Buffer  *Buffer  `json:"buffer,omitempty"`
	Storage *Storage `json:"storage,omitempty"`
	Git     *Git     `json:"git,omitempty"`
	K8s     *K8s     `json:"k8s,omitempty"`
	Cluster *Cluster `json:"cluster,omitempty"`
	Code    int      `json:"code,omitempty"`
	Msg     string   `json:"msg,omitempty"`
}

// Buffer struct
type Buffer struct {
	ID     string   `json:"id,omitempty"`
	Queues []Queue  `json:"queues,omitempty"`
	Task   *APIData `json:"task,omitempty"`
	// Jobs *int     `json:"jobs,omitempty"`
	// Action  string   `json:"action,omitempty"`
}

// Storage struct before consul
type Storage struct {
	Project         *Project    `json:"project,omitempty"`
	Projects        []Project   `json:"projects,omitempty"`
	Build           *Build      `json:"build,omitempty"`
	EcrURL          string      `json:"ecrurl,omitempty"`
	SuffixIngDomain string      `json:"suffixingdomain,omitempty"`
	Brand           *Brand      `json:"brand,omitempty"`
	Brands          []Brand     `json:"brands,omitempty"` // maybe its better to use only array to add even one brand
	TTL             *int        `json:"ttl,omitempty"`
	BuildLimit      *int        `json:"buildlimit,omitempty"`
	ServiceAccount  string      `json:"serviceaccount,omitempty"`
	SSH             *SSH        `json:"ssh,omitempty"`
	SSHs            []SSH       `json:"sshs,omitempty"`
	Git             *Git        `json:"git,omitempty"`
	Gits            []Git       `json:"gits,omitempty"`
	YAMLConfig      *YAMLConfig `json:"yamlconfig,omitempty"`
	MaintenanceMode *bool       `json:"maintenancemode,omitempty"`
	// YamlFile []byte `json:"yamlfile,omitempty"`
}

// YAMLConfig descrie yaml config
type YAMLConfig struct {
	ProjectID  string      `json:"projectid,omitempty"`
	File       []byte      `json:"file,omitempty"`
	Aviable    bool        `json:"aviable,omitempty"`
	Containers []Container `json:"containers,omitempty"`
	UseCache   bool        `json:"usecache,omitempty"` //needt to be changed to 'usecache' => 'cache'
}

// Container describe struct
type Container struct {
	Type    string              `json:"type,omitempty"`
	Name    string              `json:"name,omitempty"`
	Public  bool                `json:"public,omitempty"`
	Request *ContainerResources `json:"request,omitempty"`
	Limits  *ContainerResources `json:"limits,omitempty"`
	Status  *Status             `json:"status,omitempty"`
	Logs    *Logs               `json:"logs,omitempty"`
}

// ContainerResources descrive resources for containers
type ContainerResources struct {
	CPU    string `json:"cpu,omitempty"`
	Memory string `json:"memory,omitempty"`
}

type Logs struct {
	Follow     bool   `json:"follow,omitempty"`
	LimitBytes *int64 `json:"limitbytes,omitempty"`
	Log        []byte `json:"log,omitempty"`
}

// type Git struct {
// 	ID string `json:"id,omitempty"`
// 	Commit string `json:"commmit,omitempty"`
// 	SSHID string `json:"commmit,omitempty"`
// 	SSHKey []byte `json:"sshkey,omitempty"`
// }

// Project struct
type Project struct {
	ProjectID string `json:"projectid,omitempty"` // ?? two time ID ?
	ID        string `json:"id,omitempty"`
	Name      string `json:"name,omitempty"`
	Git       *Git   `json:"git,omitempty"`
	Transport string `json:"transport,omitempty"`
	ExtraEnv  string `json:"extraenv,omitempty"`
	SSHID     string `json:"sshid,omitempty"`
	// GitProvider string `json:"gitprovider,omitempty"`  // butbucket/Gitlab etc for comits
}

// Brand struct
type Brand struct {
	ProjectID string `json:"projectid,omitempty"`
	ID        string `json:"id,omitempty"`
	Name      string `json:"name,omitempty"`
	ExtraEnv  string `json:"extraenv,omitempty"`
}

// Build struct
type Build struct {
	Brand     *Brand  `json:"brand,omitempty"`
	BuildInt  *int    `json:"buildint,omitempty"`
	BuildsInt []int   `json:"buildsint,omitempty"`
	Project   string  `json:"project,omitempty"`
	Stages    []Stage `json:"stages,omitempty"`
	Stage     *Stage  `json:"stage,omitempty"`
}

// Stage struct
type Stage struct {
	Name      string     `json:"name,omitempty"`
	StartTime *time.Time `json:"starttime,omitempty"`
	EndTime   *time.Time `json:"endtime,omitempty"`
	Status    *Status    `json:"status,omitempty"`
	Message   string     `json:"message,omitempty"`
}

// Git struct for transport information about GIT Repo
type Git struct {
	ID           string   `json:"id,omitempty"`
	URL          string   `json:"url,omitempty"`
	Type         string   `json:"type,omitempty"`
	Project      string   `json:"project,omitempty"`
	Repo         string   `json:"repo,omitempty"`
	CommitStruct string   `json:"commitstruct,omitempty"`
	ProjectID    string   `json:"projectid,omitempty"`
	BranchID     string   `json:"branchid,omitempty"`
	Branches     []Branch `json:"branches,omitempty"`
	UseCache     bool     `json:"usecache,omitempty"`
	SSHID        string   `json:"sshid,omitempty"`
}

type SSH struct {
	ID     string `json:"id,omitempty"`
	SSHKey []byte `json:"sshkey,omitempty"`
}

// Branch struct for collect infomation about branches
type Branch struct {
	Name string `json:"name,omitempty"`
	Hash string `json:"hash,omitempty"`
}

// Queue struct for single queue description
type Queue struct {
	Name string `json:"name,omitempty"`
	Jobs *int   `json:"jobs,omitempty"`
}

// K8s struct
type K8s struct {
	Ingress         []Ingress   `json:"ingress,omitempty"`
	Ingres          *Ingress    `json:"ingres,omitempty"`
	Services        []Services  `json:"services,omitempty"`
	Service         *Services   `json:"service,omitempty"`
	Pods            []Pods      `json:"pods,omitempty"`
	Pod             *Pods       `json:"pod,omitempty"`
	Nodes           []Nodes     `json:"nodes,omitempty"`
	Node            *Nodes      `json:"node,omitempty"`
	Namespaces      []Namespace `json:"namespaces,omitempty"`
	Namespace       *Namespace  `json:"namespace,omitempty"`
	ConfigMaps      []ConfigMap `json:"configmaps,omitempty"`
	ConfigMap       *ConfigMap  `json:"configmap,omitempty"`
	Secrets         []Secret    `json:"secrets,omitempty"`
	Secret          *Secret     `json:"secret,omitempty"`
	Deploys         []Deploy    `json:"deploys,omitempty"`
	Deploy          *Deploy     `json:"deploy,omitempty"`
	Logs            *Logs       `json:"logs,omitempty"`
	AddHostIngToPod bool        `json:"addhostingtopod,omitempty"`
	LabelSearch     string      `json:"labelsearch,omitempty"`
	NamespaceID     string      `json:"namespaceid,omitempty"`
	PodID           string      `json:"podid,omitempty"`
	IngressID       string      `json:"ingressid,omitempty"`
	ServiceID       string      `json:"serviceid,omitempty"`
	ConfigMapID     string      `json:"configmapid,omitempty"`
	SecretID        string      `json:"secretid,omitempty"`
	DeployID        string      `json:"deployid,omitempty"`
	ContainerID     string      `json:"containerid,omitempty"`
	DryRun          bool        `json:"dryrun,omitempty"`
}

// Ingress struct
type Ingress struct {
	Name        string            `json:"name,omitempty"`
	Host        string            `json:"host,omitempty"`
	Namespace   string            `json:"namespace,omitempty"`
	Labels      *Labels           `json:"labels,omitempty"`
	LabelsMap   map[string]string `json:"labelsmap,omitempty"`
	StartTime   *time.Time        `json:"starttime,omitempty"`
	ServiceName string            `json:"serviceName,omitempty"`
	Deleted     bool              `json:"deleted,omitempty"`
}

// Services struct
type Services struct {
	Name      string            `json:"name,omitempty"`
	Namespace string            `json:"namespace,omitempty"`
	Labels    *Labels           `json:"labels,omitempty"`
	LabelsMap map[string]string `json:"labelsmap,omitempty"`
	EndPoint  string            `json:"endpoint,omitempty"`
	StartTime *time.Time        `json:"starttime,omitempty"`
	Deleted   bool              `json:"deleted,omitempty"`
}

// Pods struct
type Pods struct {
	Name      string            `json:"name,omitempty"`
	Namespace string            `json:"namespace,omitempty"`
	Labels    *Labels           `json:"labels,omitempty"`
	LabelsMap map[string]string `json:"labelsmap,omitempty"`
	// Status       *Status           `json:"status,omitempty"`
	PodStatus    *PodStatus  `json:"podstatus,omitempty"`
	Containers   []Container `json:"containers,omitempty"`
	StartTime    *time.Time  `json:"starttime,omitempty"`
	Host         string      `json:"host,omitempty"`
	NodeName     string      `json:"nodename,omitempty"`
	RestartCount *int        `json:"restartcount,omitempty"`
	Deleted      bool        `json:"deleted,omitempty"`
}

type PodStatus struct {
	Initialized     *StepStatus `json:"initialized,omitempty"`
	Ready           *StepStatus `json:"ready,omitempty"`
	ContainersReady *StepStatus `json:"containersready,omitempty"`
	PodScheduled    *StepStatus `json:"podscheduled,omitempty"`
	Phase           string      `json:"phase,omitempty"`
}

type StepStatus struct {
	Status             string     `json:"status,omitempty"`
	Reason             string     `json:"reason,omitempty"`
	Message            string     `json:"exitcode,omitempty"`
	LastProbeTime      *time.Time `json:"lastProbeTime,omitempty"`
	LastTransitionTime *time.Time `json:"lastTransitionTime,omitempty"`
}

type Status struct {
	ExitCode *int   `json:"exitcode,omitempty"`
	Status   string `json:"status,omitempty"`
	Ready    *bool  `json:"ready,omitempty"`
	Waiting  bool   `json:"waiting,omitempty"`
}

// Nodes struct
type Nodes struct {
	Name           string            `json:"name,omitempty"`
	CreateTime     *time.Time        `json:"starttime,omitempty"`
	LabelsMap      map[string]string `json:"labelsmap,omitempty"`
	Annotations    map[string]string `json:"annotations,omitempty"`
	Conditions     *Conditions       `json:"conditions,omitempty"`
	KubeletVersion string            `json:"kubeletversion,omitempty"`
	StatusNode     *StatusNode       `json:"statusnode,omitempty"`
}

// Namespace struct
type Namespace struct {
	Name      string            `json:"name,omitempty"`
	Labels    *Labels           `json:"labels,omitempty"`
	LabelsMap map[string]string `json:"labelsmap,omitempty"`
	StartTime *time.Time        `json:"starttime,omitempty"`
	Status    *Status           `json:"status,omitempty"`
	Deleted   bool              `json:"deleted,omitempty"`
}

// ConfigMap struct
type ConfigMap struct {
	Name       string            `json:"name,omitempty"`
	Namespace  string            `json:"namespace,omitempty"`
	Data       map[string]string `json:"data,omitempty"`
	BinaryData map[string][]byte `json:"binarydata,omitempty"`
	Immutable  *bool             `json:"immutable,omitempty"`
	StartTime  *time.Time        `json:"starttime,omitempty"`
	Labels     *Labels           `json:"labels,omitempty"`
	LabelsMap  map[string]string `json:"labelsmap,omitempty"`
	Deleted    bool              `json:"deleted,omitempty"`
}

//Secret struct
type Secret struct {
	Name       string            `json:"name,omitempty"`
	Namespace  string            `json:"namespace,omitempty"`
	Data       map[string][]byte `json:"data,omitempty"`
	StringData map[string]string `json:"stringdata,omitempty"`
	Type       string            `json:"type,omitempty"`
	Immutable  *bool             `json:"immutable,omitempty"`
	StartTime  *time.Time        `json:"starttime,omitempty"`
	Labels     *Labels           `json:"labels,omitempty"`
	LabelsMap  map[string]string `json:"labelsmap,omitempty"`
	Deleted    bool              `json:"deleted,omitempty"`
}

type Deploy struct {
	Name      string            `json:"name,omitempty"`
	Namespace string            `json:"namespace,omitempty"`
	StartTime *time.Time        `json:"starttime,omitempty"`
	Replicas  *int32            `json:"replicas,omitempty"`
	Selector  map[string]string `json:"selector,omitempty"`
	Pods      *Pods             `json:"pods,omitempty"`
	Labels    *Labels           `json:"labels,omitempty"`
	LabelsMap map[string]string `json:"labelsmap,omitempty"`
	Deleted   bool              `json:"deleted,omitempty"`
}

// StatusNode struct
type StatusNode struct {
	Capacity    *Resources `json:"capacity,omitempty"`
	Allocatable *Resources `json:"allocatable,omitempty"`
}

// Resources struct
type Resources struct { // <-has to be changed to NODE resources
	CPU    string `json:"cpu,omitempty"`
	Memory string `json:"memory,omitempty"`
	Pods   *int   `json:"pods,omitempty"`
	Disk   string `json:"disk,omitempty"`
}

// Conditions struct
type Conditions struct {
	MemoryPressure *Pressure `json:"memorypressure,omitempty"`
	DiskPressure   *Pressure `json:"diskpressure,omitempty"`
	PIDPressure    *Pressure `json:"pidpressure,omitempty"`
	Ready          *Pressure `json:"ready,omitempty"`
}

// Pressure struct
type Pressure struct {
	Type    string  `json:"type,omitempty"`
	Status  *Status `json:"status,omitempty"`
	Reason  string  `json:"reason,omitempty"`
	Message string  `json:"message,omitempty"`
}

// Labels description of labels
type Labels struct {
	App      string `json:"app,omitempty"`
	Cluster  string `json:"cluster,omitempty"`
	IDTask   string `json:"idtask,omitempty"`
	Owner    string `json:"owner,omitempty"`
	TTL      string `json:"ttl,omitempty"`
	Type     string `json:"type,omitempty"`
	Branch   string `json:"branch,omitempty"`
	Brand    string `json:"brand,omitempty"`
	Hash     string `json:"hash,omitempty"`
	Project  string `json:"project,omitempty"`
	Constant string `json:"constant,omitempty"`
	Deleting string `json:"deleting,omitempty"`
}

// Cluster struct for store info about cluster
type Cluster struct {
	Name     string `json:"name,omitempty"`
	Owner    string `json:"owner,omitempty"`
	TTL      *int   `json:"ttl,omitempty"`
	Type     string `json:"type,omitempty"`
	Constant bool   `json:"constant,omitempty"`
	Boxes    []Box  `json:"boxes,omitempty"`
	Box      *Box   `json:"box,omitempty"`
	Brand    string `json:"brand,omitempty"`
}

// Box struct to store info about specific box/pod in cluster
type Box struct {
	Branch       *Branch   `json:"branch,omitempty"`
	Brand        string    `json:"brand,omitempty"`
	Build        *int      `json:"build,omitempty"`
	Project      string    `json:"project,omitempty"`
	ExtraEnv     string    `json:"extraenv,omitempty"`
	ExtraEnvByte []byte    `json:"extraenvbyte,omitempty"`
	BaseAuth     *BaseAuth `json:"baseauth,omitempty"`
}

// BaseAuth is to collect Basing Struct information
type BaseAuth struct {
	User      string `json:"user,omitempty"`
	Password  string `json:"password,omitempty"`
	PublicURL string `json:"publicurl,omitempty"`
}

// // ContainerConfig description of container config
// type ContainerConfig struct {
// 	ID      *int              `yaml:"id"`
// 	Type    string            `yaml:"type"`
// 	Name    string            `yaml:"name"`
// 	Image   string            `yaml:"img"`
// 	Command []string          `yaml:"cmd"`
// 	Args    []string          `yaml:"args"`
// 	Volumen []int             `yaml:"vol"`
// 	Envs    map[string]string `yaml:"envs"`
// 	CPU     map[string]string `yaml:"cpu"`
// 	Memory  map[string]string `yaml:"mem"`
// 	Ports   map[string]int32  `yaml:"ports"`
// 	Public  bool              `yaml:"public"`
// 	Hide    bool              `yaml:"hide"`
// }

// // VolumenConfig description of volumen config
// type VolumenConfig struct {
// 	ID            *int   `yaml:"id"`
// 	Name          string `yaml:"name"`
// 	Type          string `yaml:"type"`
// 	MountPath     string `yaml:"mountpath"`
// 	MountSubPath  string `yaml:"mountsubpath"`
// 	MountReadOnly *bool  `yaml:"mountreadonly"`
// 	ConfigMapName string `yaml:"configmapname"`
// 	ConfigMapKey  string `yaml:"configmapkey"`
// 	ConfigMapPath string `yaml:"configmappath"`
// 	Mode          *int32 `yaml:"mode"`
// }

// // DefaultSettings description of default settings
// type DefaultSettings struct {
// 	CPU    map[string]string `yaml:"cpu"`
// 	Memory map[string]string `yaml:"mem"`
// }

// // DecodeDataJSON decode data
// func DecodeDataJSON(bArray []byte) (Data, error) {
// 	var dataD Data

// 	err := json.Unmarshal(bArray, &dataD)

// 	return dataD, err
// }

// DecodeAPIJSON decode APIJSON
func DecodeAPIJSON(bArray []byte) (APIData, error) {
	var api APIData

	err := json.Unmarshal(bArray, &api)

	return api, err
}

// VerifyK8s check if K8s struct was initialized
func (k *K8s) VerifyStruct() *K8s {
	if k == nil {
		return &K8s{}
	}
	return k
}

func (g *Git) VerifyStruct() *Git {
	if g == nil {
		return &Git{}
	}
	return g
}

func (s *Storage) VerifyStruct() *Storage {
	if s == nil {
		return &Storage{}
	}
	return s
}

func (b *Buffer) VerifyStruct() *Buffer {
	if b == nil {
		return &Buffer{}
	}
	return b
}

func (p *Project) VerifyStruct() *Project {
	if p == nil {
		return &Project{}
	}
	return p
}

func (b *Brand) VerifyStruct() *Brand {
	if b == nil {
		return &Brand{}
	}
	return b
}

func (y *YAMLConfig) VerifyStruct() *YAMLConfig {
	if y == nil {
		return &YAMLConfig{}
	}
	return y
}

func (c *Container) VerifyStruct() *Container {
	if c == nil {
		return &Container{}
	}
	return c
}

func (c *Cluster) VerifyStruct() *Cluster {
	if c == nil {
		return &Cluster{}
	}
	return c
}

func (b *Box) VerifyStruct() *Box {
	if b == nil {
		return &Box{}
	}

	return b
}

func (b *Logs) VerifyStruct() *Logs {
	if b == nil {
		return &Logs{}
	}

	return b
}

func (s *Status) VerifyStruct() *Status {
	if s == nil {
		return &Status{}
	}

	return s
}

func (p *PodStatus) VerifyStruct() *PodStatus {
	if p == nil {
		return &PodStatus{}
	}

	return p
}

func (s *SSH) VerifyStruct() *SSH {
	if s == nil {
		return &SSH{}
	}

	return s
}
