package payload

import (
	"bytes"
	"encoding/json"
	"testing"
	"time"
)

const (
	timeCheck    = "2018-08-13T01:02:03-00:00"
	storageInt   = 155
	dataInt      = 10
	apiInt       = 404
	queueInt     = 17
	labelsInt    = 78
	podsInt      = 133
	resourceInit = 55
	clusterInt   = 99
	boxInt       = 333
)

// func getFullDataPayload() Data {
// 	// dFull := dataInt
// 	dataFull := Data{
// 		Idtask:   "idtask",
// 		// Branch:   "branch",
// 		// Brand:    "brand",
// 		// Project:  "project",
// 		// Build:    &dFull,
// 		// Owner:    "owner",
// 		// TTL:      "ttl",
// 		// Type:     "type",
// 		// ExtraEnv: "extraenv",
// 		// ExtraEnv: map[string]string{"test": "test"},
// 	}
// 	return dataFull
// }

func getFullStoragePayload() Storage {

	// sshkey := []byte("sshkey")
	// yamlfile := []byte("yamlfile")
	project := getFullDataProject()
	build := getFullDataBuild()
	brand := getFullDataBrand()
	yamlConfig := getFullYamlConfig()

	storageFull := Storage{
		Project:         &project,
		Projects:        []Project{project},
		Build:           &build,
		EcrURL:          "ecrurl",
		SuffixIngDomain: "suffixingdomain",
		Brand:           &brand,
		Brands:          []Brand{brand},
		// SSHKey:          sshkey,
		YAMLConfig:      &yamlConfig,
	}
	return storageFull
}

func getFullDataProject() Project {

	project := Project{
		ProjectID: "projectid",
		ID:        "id",
		Name:      "name",
		// GitURL:    "giturl",
		Transport: "transport",
		ExtraEnv:  "extraenv",
	}

	return project
}

func getFullDataBuild() Build {

	buildInt := storageInt
	brand := getFullDataBrand()
	stage := getFullDataStage()

	build := Build{
		Project:   "project",
		BuildInt:  &buildInt,
		BuildsInt: []int{int(storageInt)},
		Brand:     &brand,
		Stage:     &stage,
		Stages:    []Stage{stage},
	}

	return build
}

func getFullDataBrand() Brand {
	brand := Brand{
		ID:        "id",
		Name:      "name",
		ExtraEnv:  "extraenv",
		ProjectID: "projectid",
	}
	return brand
}

func getFullYamlConfig() YAMLConfig {
	yamlfile := []byte("yamlfile")
	containers := []Container{getFullContainer()}

	yamlConfig := YAMLConfig{
		Aviable:    true,
		File:       yamlfile,
		Containers: containers,
		UseCache:   true,
		ProjectID:  "projectid",
	}

	return yamlConfig
}

func getFullContainer() Container {
	resource := getContainerResources()

	container := Container{
		Type:    "type",
		Name:    "name",
		Public:  true,
		Limits:  &resource,
		Request: &resource,
	}

	return container
}

func getContainerResources() ContainerResources {

	resource := ContainerResources{
		CPU:    "cpu",
		Memory: "mem",
	}

	return resource
}

func getFullDataStage() Stage {
	p, _ := time.Parse(time.RFC3339, timeCheck)

	stage := Stage{
		Name:      "name",
		// Status:    "status",
		Message:   "message",
		EndTime:   &p,
		StartTime: &p,
	}

	return stage
}

func getFullAPIPayload() APIData {
	// data := getFullDataPayload()
	storage := getFullStoragePayload()
	git := getFullGitPayload()
	// queue := getFullQueuePayload()
	k8s := getFullK8sPayload()
	cluster := getFullClusterPayload()
	buffer := getFullBufferPayload()
	// box := getFullBoxPayload()

	apiFull := APIData{
		Action: "test",
		IDtask: "idtask",
		// Data:    &data,
		Code:    apiInt,
		Msg:     "test",
		Storage: &storage,
		Git:     &git,
		Buffer:  &buffer,
		K8s:     &k8s,
		Cluster: &cluster,
	}

	return apiFull
}

func getFullClusterPayload() Cluster {
	dTTL := clusterInt
	boxes := getFullBoxesPayload()
	cluster := Cluster{
		Name:  "name",
		Owner: "owner",
		TTL:   &dTTL,
		Type:  "type",
		Boxes: boxes,
	}
	return cluster

}

func getFullBoxesPayload() []Box {
	dBox := boxInt
	baseauth := getFullBaseAuthPayload()
	branch := getFullBranchPayload()

	boxes := []Box{
		Box{
			Branch:   &branch,
			Brand:    "brand",
			Build:    &dBox,
			Project:  "project",
			ExtraEnv: "extraenv",
			BaseAuth: &baseauth,
		},
	}

	return boxes
}

func getFullBaseAuthPayload() BaseAuth {

	baseauth := BaseAuth{
		User:     "user",
		Password: "password",
	}

	return baseauth
}

func getFullGitPayload() Git {

	branch := getFullBranchPayload()

	git := Git{
		ProjectID: "projectif",
		BranchID:  "branchid",
		Branches:  []Branch{branch},
		UseCache:  true,
	}

	return git
}

func getFullBranchPayload() Branch {
	branch := Branch{
		Name: "name",
		Hash: "hash",
	}

	return branch
}

func getFullBufferPayload() Buffer {
	queue := getFullQueuePayload()
	buffer := Buffer{
		ID:     "id",
		Queues: []Queue{queue},
	}

	return buffer
}

func getFullQueuePayload() Queue {
	d := queueInt

	queue := Queue{
		Name: "Name",
		Jobs: &d,
	}

	return queue
}

func getFullK8sPayload() K8s {
	services := getFullServicesPayload()
	ingresses := getFullIngressesPayload()
	pods := getFullPodsPayload()
	nodes := getFullNodesPayload()

	k8s := K8s{
		NamespaceID:     "NamespaceID",
		AddHostIngToPod: true,
		DryRun:          true,
		LabelSearch:     "LabelSearch",
		Services:        services,
		Ingress:         ingresses,
		Pods:            pods,
		Nodes:           nodes,
	}

	return k8s
}

func getFullLabelsPayload() Labels {
	labels := Labels{
		Type:    "Type",
		TTL:     "TTL",
		Hash:    "Hash",
		Owner:   "Owner",
		IDTask:  "IDTask",
		Cluster: "Cluster",
		Brand:   "Brand",
		Project: "Project",
		Branch:  "Branch",
		App:     "App",
	}

	return labels
}

func getFullServicesPayload() []Services {
	labels := getFullLabelsPayload()
	p, _ := time.Parse(time.RFC3339, timeCheck)

	service := []Services{
		Services{
			Name:      "Name",
			Deleted:   true,
			EndPoint:  "EndPoint",
			Namespace: "Namespace",
			Labels:    &labels,
			LabelsMap: map[string]string{"test": "test"},
			StartTime: &p,
		},
	}

	return service
}

func getFullIngressesPayload() []Ingress {
	labels := getFullLabelsPayload()
	p, _ := time.Parse(time.RFC3339, timeCheck)

	ingresses := []Ingress{
		Ingress{
			Host:        "Host",
			Deleted:     true,
			Labels:      &labels,
			LabelsMap:   map[string]string{"test": "test"},
			Name:        "Name",
			Namespace:   "Namespace",
			ServiceName: "ServiceName",
			StartTime:   &p,
		}}

	return ingresses
}

func getFullPodsPayload() []Pods {
	labels := getFullLabelsPayload()
	p, _ := time.Parse(time.RFC3339, timeCheck)
	d := podsInt

	pods := []Pods{
		Pods{
			Deleted:      true,
			Labels:       &labels,
			LabelsMap:    map[string]string{"test": "test"},
			Name:         "Name",
			Namespace:    "Namespace",
			Host:         "Host",
			NodeName:     "NodeName",
			RestartCount: &d,
			// Status:       "Status",
			StartTime:    &p,
		}}

	return pods
}

func getFullNodesPayload() []Nodes {
	p, _ := time.Parse(time.RFC3339, timeCheck)
	status := getFullStatusNodePayload()

	nodes := []Nodes{
		Nodes{
			Annotations:    map[string]string{"test": "test"},
			CreateTime:     &p,
			LabelsMap:      map[string]string{"test": "test"},
			KubeletVersion: "KubeletVersion",
			Name:           "Name",
			StatusNode:     &status,
		}}

	return nodes
}

func getFullStatusNodePayload() StatusNode {
	c := getFullResourcecsPayload()
	a := getFullResourcecsPayload()

	statusNodes := StatusNode{
		Capacity:    &c,
		Allocatable: &a,
	}

	return statusNodes
}

func getFullResourcecsPayload() Resources {
	d := resourceInit

	resource := Resources{
		CPU:    "CPU",
		Disk:   "Disk",
		Memory: "Memory",
		Pods:   &d,
	}

	return resource
}

func compareFullContainerResources(result *ContainerResources, t *testing.T) {

	if result.CPU != "cpu" {
		t.Error("ContainerResources result.CPU")
	}

	if result.Memory != "memory" {
		t.Error("ContainerResources result.Memory")
	}
}

func compareFullContainers(result []Container, t *testing.T) {

	for _, val := range result {
		if val.Type != "type" {
			t.Error("container result.Type")
		}

		if val.Name != "name" {
			t.Error("container result.name")
		}

		if val.Public != true {
			t.Error("container result.Public")
		}

	}

}

func compareFullAPI(result *APIData, data *APIData, t *testing.T) {
	if result.Action != data.Action {
		t.Error("api result.Action != data.Action")
	}

	if result.Code != apiInt {
		t.Error("api *result.Code != 404")
	}

	if result.Msg != data.Msg {
		t.Error("api result.Msg != data.Msg")
	}

}

func compareFullYamlConfig(result *YAMLConfig, t *testing.T) {

	resYaml := bytes.Compare(result.File, []byte("yamlfile"))
	if resYaml != 0 {
		t.Error("yamlConfig bytes.Compare(*result.YamlFile,[]byte(yamlfile)")
	}

	if result.Aviable != true {
		t.Error("yamlConfig result.Aviable")
	}

	if result.UseCache != true {
		t.Error("yamlConfig result.UseCache")
	}

}

func compareFullStorage(result *Storage, t *testing.T) {

	if result.EcrURL != "ecrurl" {
		t.Error("storage result.EcrURL != ecrurl")
	}

	// p, _ := time.Parse(time.RFC3339, timeCheck)

	// if !result.EndTime.Equal(p) {
	// 	t.Error("storage result.EndTime.Equal(p)")
	// }

	// if result.GitURL != "giturl" {
	// 	t.Error("storage result.GitURL != giturl")
	// }

	// if result.Project != "project" {
	// 	t.Error("storage result.Project != project")
	// }

	// for _, val := range result.Projects {
	// 	if val != "projects" {
	// 		t.Error("storage val != projects")
	// 	}
	// }

	// resSSH := bytes.Compare(result.SSHKey, []byte("sshkey"))
	// if resSSH != 0 {
	// 	t.Error("storage bytes.Compare(test, *result.SSHKey)")
	// }

	// for _, val := range result.Stages {
	// 	if val != "stages" {
	// 		t.Error("fustoragell if val != stages")
	// 	}
	// }

	// if !result.StartTime.Equal(p) {
	// 	t.Error("storage !result.StartTime.Equal(p) ")
	// }

	// if result.Status != "status" {
	// 	t.Error("storage result.Status != status")
	// }

	if result.SuffixIngDomain != "suffixingdomain" {
		t.Error("storage result.SuffixIngDomain != suffixingdomain")
	}

	// resYaml := bytes.Compare(result.YamlFile, []byte("yamlfile"))
	// if resYaml != 0 {
	// 	t.Error("storage bytes.Compare(*result.YamlFile,[]byte(yamlfile)")
	// }

}

func compareFullProjest(result *Project, t *testing.T) {
	if result.ID != "id" {
		t.Error("project result.ID")
	}

	if result.Name != "name" {
		t.Error("project result.Name")
	}

	// if result.GitURL != "giturl" {
	// 	t.Error("project result.GitURL")
	// }

	if result.Transport != "transport" {
		t.Error("project result.Transport")
	}

	if result.ExtraEnv != "extraenv" {
		t.Error("project result.ExtraEnv")
	}
}

func compareFullBrand(result *Brand, t *testing.T) {
	if result.ID != "id" {
		t.Error("brand result.ID")
	}

	if result.Name != "name" {
		t.Error("brand result.Name")
	}

	if result.ProjectID != "projectid" {
		t.Error("brand result.ProjectID")
	}

	if result.ExtraEnv != "extraenv" {
		t.Error("brand result.ExtraEnv")
	}
}

func compareFullStage(result *Stage, t *testing.T) {
	p, _ := time.Parse(time.RFC3339, timeCheck)

	if result.Name != "name" {
		t.Error("stage result.Name")
	}

	if !result.StartTime.Equal(p) {
		t.Error("stage result.StartTime")
	}

	if !result.EndTime.Equal(p) {
		t.Error("stage result.EndTime")
	}

	// if result.Status != "status" {
	// 	t.Error("stage result.Status")
	// }

	if result.Message != "message" {
		t.Error("stage result.Message")
	}
}

func compareFullBuild(result *Build, t *testing.T) {
	if *result.BuildInt != storageInt {
		t.Error("build *result.BuildInt == storageInt")
	}

	for _, val := range result.BuildsInt {
		if val != storageInt {
			t.Error("build val != storageInt")
		}
	}

	if result.Project != "project" {
		t.Error("build result.Projec")
	}
}

func compareFullGit(result *Git, t *testing.T) {

	if result.BranchID != "projectid" {
		t.Error("git result.Project")
	}

	for _, val := range result.Branches {
		if val.Name != "Name" {
			t.Error("git val.Name")
		}

		if val.Hash != "hash" {
			t.Error("git val.Hash")
		}
	}

	if result.UseCache != true {
		t.Error("git result.UseCache")
	}

	if result.BranchID != "branchid" {
		t.Error("git result.Branch")
	}

	// if result.UpdateCache != true {
	// 	t.Error("git  result.UpdateCache ")
	// }
}

func compareFullCluster(result *Cluster, t *testing.T) {
	if result.Name != "name" {
		t.Error("cluster result.Name")
	}

	if result.Owner != "owner" {
		t.Error("cluster result.owner")
	}

	if *result.TTL != clusterInt {
		t.Error("cluster result.TTL")
	}

	if result.Type != "type" {
		t.Error("cluster result.Type")
	}
}

func compareFullBoxes(result []Box, t *testing.T) {
	for _, val := range result {
		if val.Project != "project" {
			t.Error("boxes val.Project")
		}

		if val.Brand != "brand" {
			t.Error("boxes val.Brand")
		}

		// if val.Branch != "branch" {
		// 	t.Error("boxes val.Branch")
		// }

		if *val.Build != boxInt {
			t.Error("boxes val.Build")
		}
	}

}

func compareFullBuffor(result *Buffer, t *testing.T) {

	if result.ID != "id" {
		t.Error("buffer  result.ID")
	}
}

func compareFullQueue(result []Queue, t *testing.T) {

	for _, val := range result {

		if *val.Jobs != queueInt {
			t.Error("queue *result.Jobs != queueInt")
		}

		if val.Name != "Name" {
			t.Error("queue result.Name != Name")
		}
	}

}

func compareFullPods(result *[]Pods, t *testing.T) {

	for _, val := range *result {

		if val.Deleted != true {
			t.Error("pods result.Deleted != true")
		}

		if val.Host != "Host" {
			t.Error("pods result.Host")
		}

		if val.LabelsMap["test"] != "test" {
			t.Error("pods result.LabelsMap")
		}

		if val.Name != "Name" {
			t.Error("pods result.Name")
		}

		if val.Namespace != "Namespace" {
			t.Error("pods result.Namespace")
		}

		if val.NodeName != "NodeName" {
			t.Error("pods result.NodeName")
		}

		if *val.RestartCount != podsInt {
			t.Error("pods result.RestartCount")
		}

		p, _ := time.Parse(time.RFC3339, timeCheck)

		if !val.StartTime.Equal(p) {
			t.Error("pods  *result.StartTime")
		}

		// if val.Status != "Status" {
		// 	t.Error("pods  *result.StartTime")
		// }

		compareFullLabels(val.Labels, t)
	}

}

func compareFullServices(result *[]Services, t *testing.T) {

	for _, val := range *result {

		if val.Deleted != true {
			t.Error("Ingress result.Deleted != true")
		}

		if val.LabelsMap["test"] != "test" {
			t.Error("Ingress result.LabelsMap")
		}

		if val.Name != "Name" {
			t.Error("Ingress result.Name")
		}

		if val.Namespace != "Namespace" {
			t.Error("Ingress result.Namespace")
		}

		if val.EndPoint != "EndPoint" {
			t.Error("Ingress result.NodeName")
		}

		p, _ := time.Parse(time.RFC3339, timeCheck)

		if !val.StartTime.Equal(p) {
			t.Error("Ingress  *result.StartTime")
		}

		compareFullLabels(val.Labels, t)
	}
}

func compareFullIngress(result *[]Ingress, t *testing.T) {

	for _, val := range *result {

		if val.Deleted != true {
			t.Error("services result.Deleted != true")
		}

		if val.LabelsMap["test"] != "test" {
			t.Error("services result.LabelsMap")
		}

		if val.Name != "Name" {
			t.Error("services result.Name")
		}

		if val.Namespace != "Namespace" {
			t.Error("services result.Namespace")
		}

		if val.Host != "Host" {
			t.Error("services result.Host")
		}

		if val.ServiceName != "ServiceName" {
			t.Error("services result.Host")
		}

		p, _ := time.Parse(time.RFC3339, timeCheck)

		if !val.StartTime.Equal(p) {
			t.Error("services  *result.StartTime")
		}

		compareFullLabels(val.Labels, t)
	}
}

func compareFullLabels(result *Labels, t *testing.T) {
	if result.App != "App" {
		t.Error("labels result.App")
	}

	if result.Branch != "Branch" {
		t.Error("labels result.Branch")
	}

	if result.Brand != "Brand" {
		t.Error("labels result.Brand")
	}

	if result.Hash != "Hash" {
		t.Error("labels result.Hash")
	}

	if result.IDTask != "IDTask" {
		t.Error("labels result.IDTask")
	}

	if result.Owner != "Owner" {
		t.Error("labels  result.Owner")
	}

	if result.Project != "Project" {
		t.Error("labels result.Project")
	}

	if result.TTL != "TTL" {
		t.Error("labels result.TTL")
	}

	if result.Type != "Type" {
		t.Error("labels result.Type")
	}
}

// func compareFullBox(result *Box, t *testing.T) {
// 	if result.Validated != true {
// 		t.Error("box result.Validated")
// 	}
// }

func TestDecodeAPIJSON(t *testing.T) {

	data := getFullAPIPayload()
	arrayByte, _ := json.Marshal(data)
	decodeData, err := DecodeAPIJSON(arrayByte)

	if err != nil {
		t.Errorf("full DecodeDataJSON returned error %v", err.Error())
	}

	compareFullAPI(&decodeData, &data, t)
	compareFullStorage(decodeData.Storage, t)
	// compareFullb
	compareFullQueue(decodeData.Buffer.Queues, t)
	compareFullCluster(decodeData.Cluster, t)
	compareFullBoxes(decodeData.Cluster.Boxes, t)
	compareFullProjest(decodeData.Storage.Project, t)
	compareFullBuild(decodeData.Storage.Build, t)
	compareFullBrand(decodeData.Storage.Brand, t)
	compareFullStage(decodeData.Storage.Build.Stage, t)
	compareFullYamlConfig(decodeData.Storage.YAMLConfig, t)
	compareFullContainers(decodeData.Storage.YAMLConfig.Containers, t)

	apiEmpty := APIData{}
	byteAPIEmpty, _ := json.Marshal(apiEmpty)
	_, err = DecodeAPIJSON(byteAPIEmpty)

	if err != nil {
		t.Errorf("empty DecodeAPIJSON returned error %v", err.Error())
	}
}

// func TestDecodeDataJSON(t *testing.T) {

// 	data := getFullDataPayload()
// 	arrayByte, _ := json.Marshal(data)
// 	decodeData, err := DecodeDataJSON(arrayByte)

// 	if err != nil {
// 		t.Errorf("full DecodeDataJSON returned error %v", err.Error())
// 	}

// compareFullData(&decodeData, &data, t)

// dHalf := 11
// dataHalf := Data{
// 	Idtask:   "idtask",
// 	Build:    &dHalf,
// 	ExtraEnv: map[string]string{"test": "test"},
// }

// byteDataHalf, _ := json.Marshal(dataHalf)
// dataResultHalf, err := DecodeDataJSON(byteDataHalf)

// if err != nil {
// 	t.Errorf("half DecodeDataJSON returned error %v", err.Error())
// }

// if dataResultHalf.Idtask != dataHalf.Idtask {
// 	t.Error("full dataResultFull.Idtask != data.Idtask")
// }

// if *dataResultHalf.Build != 11 {
// 	t.Error("full *dataResultFull.Build != 10")
// }

// if dataResultHalf.ExtraEnv["test"] != "test" {
// 	t.Error("full dataResultFull.ExtraEnv[\"test\"] != \"test\"")
// }

// dataEmpty := Data{}
// byteDataEmpty, _ := json.Marshal(dataEmpty)
// _, err = DecodeDataJSON(byteDataEmpty)

// if err != nil {
// 	t.Errorf("empty DecodeDataJSON returned error %v", err.Error())
// }

// dataIncorect := `{"incorect":"test"}`
// _, err = DecodeDataJSON([]byte(dataIncorect))

// if err != nil {
// 	t.Errorf("incorrect DecodeDataJSON returned error %v", err.Error())
// }
// }

// func TestDecodeStorageJSON(t *testing.T) {

// 	data := getFullStoragePayload()
// 	arrayByte, _ := json.Marshal(data)
// 	decodeData, err := DecodeStorageJSON(arrayByte)

// 	if err != nil {
// 		t.Errorf("full DecodeStorageJSON returned error %v", err.Error())
// 	}

// 	compareFullStorage(&decodeData, t)
// 	compareFullProjest(decodeData.Project, t)
// 	compareFullBuild(decodeData.Build, t)
// 	compareFullBrand(decodeData.Brand, t)
// 	compareFullStage(decodeData.Build.Stage, t)
// 	compareFullYamlConfig(decodeData.YAMLConfig, t)

// }

// func TestDecodeGitJSON(t *testing.T) {

// 	data := getFullGitPayload()
// 	arrayByte, _ := json.Marshal(data)
// 	decodeData, err := DecodeGitJSON(arrayByte)

// 	if err != nil {
// 		t.Errorf("full DecodeGitJSON returned error %v", err.Error())
// 	}

// 	compareFullGit(&decodeData, t)
// }

// func TestDecodeQueueJSON(t *testing.T) {

// 	data := getFullQueuePayload()
// 	arrayByte, _ := json.Marshal(data)
// 	decodeData, err := DecodeQueueJSON(arrayByte)

// 	if err != nil {
// 		t.Errorf("full DecodeQueueJSON returned error %v", err.Error())
// 	}

// 	compareFullQueue(&decodeData, t)

// }

// func TestDecodeLabels(t *testing.T) {
// 	data := getFullLabelsPayload()
// 	arrayByte, _ := json.Marshal(data)
// 	decodeData, err := decodeLabels(arrayByte)

// 	if err != nil {
// 		t.Errorf("full decodeLabels returned error %v", err.Error())
// 	}

// 	compareFullLabels(&decodeData, t)
// }

// func TestDecodePods(t *testing.T) {
// 	data := getFullPodsPayload()
// 	arrayByte, _ := json.Marshal(data)
// 	decodeData, err := decodePods(arrayByte)

// 	if err != nil {
// 		t.Errorf("full decodePods returned error %v", err.Error())
// 	}

// 	compareFullPods(decodeData, t)
// 	// compareFullLabels(decodeData.Labels, t)
// }

// func TestDecodeSvc(t *testing.T) {
// 	data := getFullServicesPayload()
// 	arrayByte, _ := json.Marshal(data)
// 	decodeData, err := decodeSvc(arrayByte)

// 	if err != nil {
// 		t.Errorf("full decodeSvc returned error %v", err.Error())
// 	}

// 	compareFullServices(decodeData, t)
// }

// func TestDecodeIng(t *testing.T) {
// 	data := getFullIngressesPayload()
// 	arrayByte, _ := json.Marshal(data)
// 	decodeData, err := decodeIng(arrayByte)

// 	if err != nil {
// 		t.Errorf("full decodeIng returned error %v", err.Error())
// 	}

// 	compareFullIngress(decodeData, t)
// }

// func TestDecodeBox(t *testing.T) {
// 	data := getFullBoxPayload()
// 	arrayByte, _ := json.Marshal(data)
// 	decodeData, err := DecodeBoxJSON(arrayByte)

// 	if err != nil {
// 		t.Errorf("full DecodeBoxJSON returned error %v", err.Error())
// 	}

// 	compareFullBox(&decodeData, t)
// }

// func TestVerifyService(t *testing.T) {

// 	// test for empty whole struct
// 	emptyStruct := APIData{}
// 	emptyResult := VerifyService(&emptyStruct)

// 	if emptyResult == nil {
// 		t.Error("empty service Struct, test failed, got nil")
// 	}

// 	// test for initialized only K8s struct
// 	emptyHalf := APIData{
// 		K8s: &K8s{
// 			Namespace: "test",
// 		},
// 	}
// 	resultHalf := VerifyService(&emptyHalf)

// 	if resultHalf == nil {
// 		t.Error("empty Ingress Struct, test failed, got nil")
// 	}

// 	if emptyHalf.K8s.Namespace != "test" {
// 		t.Error("namespace of Ingress Struct, test failed, got nil")
// 	}

// 	// test with fully initialized
// 	b := true
// 	i := 10
// 	fullTest := APIData{
// 		K8s: &K8s{
// 			Namespace: "test",
// 			Services: []Services{
// 				Services{
// 					Name:      "test",
// 					Deleted:   &b,
// 					EndPoint:  "test",
// 					Namespace: "test",
// 					// StartTime:
// 					Labels: Labels{
// 						Type:   "box",
// 						TTL:    "7",
// 						Repo:   "test",
// 						Owner:  "test.user",
// 						IDTask: "asdfghjklzxcvbnm",
// 						ExtraEnv: map[string]string{
// 							"test": "test",
// 						},
// 						BuildInt: &i,
// 						Branch:   "test",
// 						App:      "test",
// 					},
// 				},
// 			},
// 		},
// 	}

// 	fullResult := VerifyService(&fullTest)

// 	if len(*fullResult) != 1 {
// 		t.Error("full Ingress Struct, there should be only one element ")
// 	}

// 	for _, val := range *fullResult {
// 		if val.Name != "test" {
// 			t.Error("full Ingress Struct, host should be 'test'")
// 		}

// 		if *val.Deleted != b {
// 			t.Error("full Ingress Struct, Deleted should be 'true'")
// 		}

// 		if val.Labels.Type != "box" {
// 			t.Error("full Ingress Struct, val.Labels.Type should be 'box'")
// 		}

// 		if val.Labels.TTL != "7" {
// 			t.Error("full Ingress Struct, val.Labels.TTL should be 'TTL'")
// 		}

// 		if val.Labels.Repo != "test" {
// 			t.Error("full Ingress Struct, val.Labels.Repo should be 'test'")
// 		}

// 		if val.Labels.Owner != "test.user" {
// 			t.Error("full Ingress Struct, val.Labels.Repo should be 'test.user'")
// 		}

// 		if val.Labels.IDTask != "asdfghjklzxcvbnm" {
// 			t.Error("full Ingress Struct, val.Labels.IDTask should be 'asdfghjklzxcvbnm'")
// 		}

// 		m := val.Labels.ExtraEnv
// 		if m["test"] != "test" {
// 			t.Error("full Ingress Struct, val.Labels.ExtraEnv should be 'test'")
// 		}

// 		if *val.Labels.BuildInt != 10 {
// 			t.Error("full Ingress Struct, val.Labels.BuildInt should be '10'")
// 		}

// 		if val.Labels.Branch != "test" {
// 			t.Error("full Ingress Struct, val.Labels.Branch should be 'test'")
// 		}

// 		if val.Labels.App != "test" {
// 			t.Error("full Ingress Struct, val.Labels.App should be 'test'")
// 		}

// 	}
// }

// func TestVerifyIngress(t *testing.T) {

// 	// test for empty whole struct
// 	emptyStruct := APIData{}
// 	emptyResult := VerifyIngress(&emptyStruct)

// 	if emptyResult == nil {
// 		t.Error("empty Ingress Struct, test failed, got nil")
// 	}

// 	// test for initialized only K8s struct
// 	emptyIngress := APIData{
// 		K8s: &K8s{
// 			Namespace: "test",
// 		},
// 	}
// 	ingressEmpty := VerifyIngress(&emptyIngress)

// 	if ingressEmpty == nil {
// 		t.Error("empty Ingress Struct, test failed, got nil")
// 	}

// 	if emptyIngress.K8s.Namespace != "test" {
// 		t.Error("namespace of Ingress Struct, test failed, got nil")
// 	}

// 	// test with fully initialized
// 	b := true
// 	i := 10
// 	fullIngress := APIData{
// 		K8s: &K8s{
// 			Namespace: "test",
// 			Ingress: []Ingress{Ingress{
// 				Host:    "test",
// 				Deleted: &b,
// 				Labels: Labels{
// 					Type:   "box",
// 					TTL:    "7",
// 					Repo:   "test",
// 					Owner:  "test.user",
// 					IDTask: "asdfghjklzxcvbnm",
// 					ExtraEnv: map[string]string{
// 						"test": "test",
// 					},
// 					BuildInt: &i,
// 					Branch:   "test",
// 					App:      "test",
// 				},
// 			}},
// 		},
// 	}

// 	fullResult := VerifyIngress(&fullIngress)

// 	if len(*fullResult) != 1 {
// 		t.Error("full Ingress Struct, there should be only one element ")
// 	}

// 	for _, val := range *fullResult {
// 		if val.Host != "test" {
// 			t.Error("full Ingress Struct, host should be 'test'")
// 		}

// 		if *val.Deleted != b {
// 			t.Error("full Ingress Struct, Deleted should be 'true'")
// 		}

// 		if val.Labels.Type != "box" {
// 			t.Error("full Ingress Struct, val.Labels.Type should be 'box'")
// 		}

// 		if val.Labels.TTL != "7" {
// 			t.Error("full Ingress Struct, val.Labels.TTL should be 'TTL'")
// 		}

// 		if val.Labels.Repo != "test" {
// 			t.Error("full Ingress Struct, val.Labels.Repo should be 'test'")
// 		}

// 		if val.Labels.Owner != "test.user" {
// 			t.Error("full Ingress Struct, val.Labels.Repo should be 'test.user'")
// 		}

// 		if val.Labels.IDTask != "asdfghjklzxcvbnm" {
// 			t.Error("full Ingress Struct, val.Labels.IDTask should be 'asdfghjklzxcvbnm'")
// 		}

// 		if val.Labels.ExtraEnv["test"] != "test" {
// 			t.Error("full Ingress Struct, val.Labels.ExtraEnv should be 'test'")
// 		}

// 		if *val.Labels.BuildInt != 10 {
// 			t.Error("full Ingress Struct, val.Labels.BuildInt should be '10'")
// 		}

// 		if val.Labels.Branch != "test" {
// 			t.Error("full Ingress Struct, val.Labels.Branch should be 'test'")
// 		}

// 		if val.Labels.App != "test" {
// 			t.Error("full Ingress Struct, val.Labels.App should be 'test'")
// 		}

// 	}
// }
